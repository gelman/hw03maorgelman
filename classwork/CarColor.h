#pragma once
#include <iostream>

enum Color {GREEN = 1,BLACK = 2, RED = 3,YELLOW = 4, BLUE = 5,WHITE = 6};

class CarColor
{
public:

	CarColor(const Color&);
	CarColor(); 
	Color getColor() const; 
						
	bool operator==(const CarColor& c) const;
	friend std::ostream &operator<<(std::ostream &s, const CarColor &color);

private:
	Color _color;
};

