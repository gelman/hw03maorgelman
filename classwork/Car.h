#pragma once
#include "CarColor.h"
#include <string>

class Car
{
public:

	Car(std::string owner, double price, Color color, std::string model, std::string company);

	std::string getOwner() const;
	double getPrice() const;
	CarColor getCarColor() const;
	std::string getModel() const;
	std::string getCompany() const;
	void setOwner(const std::string o);
	void setPrice(const double p);
	void setCarColor(const CarColor c);
	void setModel(const std::string m);
	void setCompany(const std::string comp);
	bool operator==(const Car &nC) const;
	bool operator<(const Car &nC) const;
	bool operator>(const Car &nC) const;
	bool operator!=(const Car &nC) const;
	int totalCars() const;
	void print() const;
	friend std::ostream &operator<<(std::ostream &s, const Car &car);

private:
	std::string _owner;
	double _price;
	CarColor _carColor;
	std::string _model;
	std::string _company;
};