#include "Car.h"



Car::Car(std::string owner, double price, Color color, std::string model, std::string company) :_owner(owner),_price(price),_carColor(color),_model(model),_company(company)
{
	
}



std::string Car::getOwner() const
{
	return this->_owner;
}




double Car::getPrice() const
{
	return this->_price;
}




CarColor Car::getCarColor() const
{
	return this->_carColor;
}

std::string Car::getModel() const
{
	return this->_model;
}




std::string Car::getCompany() const
{
	return this->_company;
}





void Car::setOwner(const std::string o)
{
	this->_owner = o;
}





void Car::setPrice(const double p)
{
	this->_price = p;
}





void Car::setCarColor(const CarColor c)
{
	this->_carColor = c;
}




void Car::setModel(const std::string m)
{ 
	this->_model = m; 
}





void Car::setCompany(const std::string comp)
{
	this->_company = comp;
}




std::ostream &operator<<(std::ostream &s, const Car &car)
{
	s<<"Model: "<<car.getModel()<<"\n"
		<<"Company: "<< car.getCompany()<<"\n"
		<<"Price: "<<car.getPrice()<<"  $\n"
		<<"Owner: "<< car.getOwner()<<"\n";


	return s;
}





bool Car::operator>(const Car &nC) const
{


	if (this->_price == nC._price)
	{

		return this->_model > nC._model;

	}
	else
	{

		return this->_price > nC._price;

	}
}


bool Car::operator==(const Car &nC) const
{
	return this->_price == nC._price && this->_model == nC._model && this->_company == nC._company && this->_carColor == nC._carColor;
}


bool Car::operator<(const Car &nC) const
{
	if (this->_price == nC._price)
	{
		return this->_model < nC._model;
	}
	else
	{
		return this->_price < nC._price;
	}
}

bool Car::operator!=(const Car &nC) const
{
	return *this != nC;
}



void Car::print() const
{
	std::cout << *this;
}