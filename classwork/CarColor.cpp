#include "CarColor.h"


CarColor::CarColor(const Color& color) : _color(color)
{
	
}



CarColor::CarColor()
{
	this->_color = Color::WHITE;
}




Color CarColor::getColor() const
{
	return this->_color;
}




bool CarColor::operator==(const CarColor &c) const
{

	return (this->_color == c._color);

}





std::ostream &operator<<(std::ostream &s, const CarColor &color)
{
	switch (color.getColor())
	{
	case GREEN:
		s << "green";
		break;
	case BLACK:
		s << "black";
		break;
	case RED:
		s << "red";
		break;
	case YELLOW:
		s << "yellow";
		break;
	case BLUE:
		s << "blue";
		break;
	case WHITE:
		s << "white";
		break;
	}


	return s;
}