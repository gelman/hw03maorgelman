#include <iostream>
#include "CarList.h"


int main()
{
	CarList carList;

	Car firstCar("Maor", 135, Color::RED, "Corolla", "Toyota");
	Car secondCar("Lolo", 500, Color::BLACK, "C3", "Citroen");
	Car thirdCar("Rich", 234, Color::RED, "Kuga", "Ford");
	Car fourthCar("Liran", 80, Color::BLUE, "Corolla", "Toyota");


	carList.add(fourthCar);
	carList.add(secondCar);
	carList.add(firstCar);
	carList.add(thirdCar);



	carList.print();

	std::cout << "Num of red cars: " << carList.numOfRedCars() << std::endl;

	std::cout << "" << std::endl;

	carList.printMostExpansive();

	getchar();
	return 0;
}