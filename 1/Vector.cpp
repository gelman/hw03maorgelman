#include <iostream>
#include "Vector.h"

/*defines*/
#define MIN_SIZE 2
#define ERROR -9999

/*Vector Constructor*/
Vector::Vector(const int n)
{
	if (n < MIN_SIZE)
	{
		/*putting min values in the fields*/
		this->_elements = new int[MIN_SIZE];
		this->_resizeFactor = MIN_SIZE;
		this->_size = 0;
		this->_capacity = MIN_SIZE;
	}
	else
	{
		/*putting values in the fields*/
		this->_elements = new int[n];
		this->_resizeFactor = n;
		this->_size = 0;
		this->_capacity = n;
	}
}

/*Vector Destructor*/
Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}

/*getters*/
int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

/*checking if Vector is empty*/
bool Vector::empty() const
{
	return (this->_size == 0);
}


/*adding value to the end of the vector*/
void Vector::push_back(const int& val)
{
	/*variables definition*/
	int* newVector = nullptr;
	int i = 0;

	if (this->_size < this->_capacity) /*checking if we don't need change the size of the vector*/
	{
		this->_elements[this->_size] = val;
		this->_size++;
	}
	else
	{
		newVector = new int[this->_capacity + this->_resizeFactor]; /*dynamic allocation*/

		/*putting the values in the new array*/
		for (i = 0; i < this->_size; i++)
		{
			newVector[i] = this->_elements[i];
		}

		newVector[i] = val; /*adding the new value*/

		delete[] this->_elements; /*free all the data*/

		this->_elements = newVector;

		newVector = nullptr;

		this->_capacity += this->_resizeFactor; /*changing the capacity*/
		
		this->_size++;
	}
}

/*deleting value from the end of the vector*/
int Vector::pop_back() 
{
	/*variables definition*/
	int deletedVal = 0;

	if (this->_size > 0) /*checking if there is a value to delete*/
	{
		deletedVal = this->_elements[this->_size - 1];
		this->_elements[this->_size - 1] = 0;
		this->_size--;
		return deletedVal;
	}
	else /*error*/
	{
		std::cerr << "error: pop from empty vector" << std::endl;
		return ERROR;
	}
}

/*changing the capacity of the vector*/
void Vector::reserve(const int n) 
{
	/*variables definition*/
	int* newVector = nullptr;
	int howManyTimesBigeer = 0;

	if (this->_capacity < n) /*checking if the capacity is not enough*/
	{
		/*calculating how many times we need to add the resizeFactor to the capacity*/
		howManyTimesBigeer = (n - this->_capacity) / this->_resizeFactor;

		if ((n - this->_capacity) % this->_resizeFactor > 0) /*checking if we need more space*/
		{
			howManyTimesBigeer++;
		}

		this->_capacity += howManyTimesBigeer * this->_resizeFactor; /*changing the capacity according to the calculations*/

		newVector = new int[this->_capacity]; /*dynamic allocation*/

		for (int i = 0; i < this->_size; i++) /*putting the values in the new array*/
		{
			newVector[i] = this->_elements[i];
		}

		delete[] this->_elements; /*free all the data*/

		this->_elements = newVector;

		newVector = nullptr;

	}

}

/*changing the size of the vector according to n*/
void Vector::resize(const int n)
{
	/*variables definition*/
	int* newVector = nullptr;

	if (n < this->_capacity) /*we need vector with a smaller size*/
	{
		newVector = new int[n]; /*dynamic allocation*/
		
		if (n < this->_size) /*we don't need to add values*/
		{
			for (int i = 0; i < n; i++) /*putting the values in the new vector*/
			{
				newVector[i] = this->_elements[i];
			}
		}
		else /*we need to add values*/
		{
			for (int i = 0; i < this->_size; i++)  /*putting the values in the new vector*/
			{
				newVector[i] = this->_elements[i];
			}

			for (int i = this->_size; i < n; i++) /*adding values in order to get n*/
			{
				newVector[i] = 0;
			}
		}

		delete[] this->_elements; /*free all the data*/
		
		this->_elements = newVector;

		newVector = nullptr;

		this->_size = n;

	}
	else if(n > this->_capacity)/*we need more space in the vector*/
	{
		this->reserve(n); /*getting more space*/

		for (int i = this->_size; i < n; i++) /*adding vlaues in order to get n*/
		{
			this->_elements[i] = 0;
		}

		this->_size = n;
	}

}

/*changing all the values in the vector to val*/
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++) /*running over all the values in the vector*/
	{
		this->_elements[i] = val;
	}
}

/*changing the size of the vector according to n and if we need to add values we add val */
void Vector::resize(int n, const int& val)
{
	/*variables definition*/
	int* newVector = nullptr;

	if (n < this->_capacity) /*we need vector with a smaller size*/
	{
		newVector = new int[n]; 

		if (n < this->_size)
		{
			for (int i = 0; i < n; i++) /*putting the values in the new vector*/
			{
				newVector[i] = this->_elements[i];
			}
		}
		else /*we need to add values*/
		{
			for (int i = 0; i < this->_size; i++) /*putting the values in the new vector*/
			{
				newVector[i] = this->_elements[i];
			}


			for (int i = this->_size; i < n; i++) /*adding values in order to get n*/
			{
				newVector[i] = val;
			}
		}

		delete[] this->_elements; /*free all the data*/

		this->_elements = newVector;

		newVector = nullptr;

		this->_size = n;

	}
	else if (n > this->_capacity) /*we need more space in the vector*/
	{
		this->reserve(n); /*getting more space*/

		for (int i = this->_size; i < n; i++) /*adding values in order to get n*/
		{
			this->_elements[i] = val;
		}

		this->_size = n;
	}
}

/*Copy Constructor*/
Vector::Vector(const Vector& other)
{
	/*putting the right values in the fields*/
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;

	delete[] this->_elements; /*free all the date*/

	this->_elements = new int[this->_capacity]; /*dynamic allocation*/

	for (int i = 0; i < this->_size; i++) /*putting all the values in the vector*/
	{
		this->_elements[i] = other._elements[i];
	}

}


/*operator =*/
Vector& Vector::operator=(const Vector& other)
{
	if (this == &other) /*copy the object to itself*/
	{
		return *this;
	}

	/*putting the right values in the fields*/
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;

	delete[] this->_elements; /*free all the date*/

	this->_elements = new int[this->_capacity]; /*dynamic allocation*/

	for (int i = 0; i < this->_size; i++) /*putting all the values in the vector*/
	{
		this->_elements[i] = other._elements[i];
	}

	return *this;
}


/*operator [] */
int& Vector::operator[](int n) const
{

	if (n > this->_capacity - 1 || n < 0) /*checking if it is invalid index*/
	{
		/*error-invliad index*/
		std::cerr << "error: n is going out of the vector's bounds" << std::endl;
		return this->_elements[0];
	}
	else
	{
		return this->_elements[n]; /*returning the right value*/
	}

}