#include <iostream>
#include "Vector.h"

int main()
{
	int size = 10;

	Vector v(size); /*creating vector*/

	/*putting values in the vector (1 - size)*/
	for (int i = 0; i < size; i++)
	{
		v.push_back(i+1);
	}

	
	v.resize(17,90); /*changing the size of the vector and adding values*/

	Vector a = v; /*using operator = */

	for (int i = 0; i < a.size(); i++) /*printing the values of vector a*/
	{
		std::cout << a[i] << std::endl; /*using operator [] */
	}

	system("pause");
	return 0;
}